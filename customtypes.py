from collections import namedtuple

BlogPost = namedtuple("BlogPost", ["title", "date", "url"])
DlInfo = namedtuple("DlInfo", ["id", "name", "date", "size"])
DlFile = namedtuple("DlFile", ["size", "stream"])
