import itertools
import tkinter
from typing import Any, Callable, Literal

import consts
import resources

Side = Literal["left", "right", "top", "bottom"]
Anchor = Literal["nw", "n", "ne", "w", "center", "e", "sw", "s", "se"]
Fill = Literal["none", "x", "y", "both"]


class TextLabel(tkinter.Label):
    def __init__(
        self,
        parent: tkinter.Misc,
        text: str,
        font: str = consts.FONTS.LABEL,
        fontsize: int = 20,
        color: str = consts.COLORS.WHITE,
        image: tkinter.PhotoImage | None = None,
    ) -> None:
        super().__init__(
            parent,
            text=text,
            font=f"{font} {fontsize}",
            fg=color,
            bg=consts.COLORS.BACKGROUND,
            image=image,
        )


class BorderedFrame(tkinter.Frame):
    def __init__(
        self,
        parent: tkinter.Misc,
        color: str = consts.COLORS.WHITE,
        size: float = 2,
        padx: int = 2,
        pady: int = 2,
        **kwargs: Any,
    ) -> None:
        self.size = size
        self.padx = padx
        self.pady = pady
        super().__init__(parent, bg=color, **kwargs)
        self.content = tkinter.Frame(
            self, bg=consts.COLORS.BACKGROUND, **kwargs
        )

    def clear(self) -> None:
        for child in self.content.winfo_children():
            child.destroy()

    def pack(self, **kwargs):
        super().pack(pady=self.pady, padx=self.padx, **kwargs)
        self.content.pack(pady=self.size, padx=self.size, **kwargs)


class BorderedButton(BorderedFrame):
    def __init__(
        self,
        parent: tkinter.Misc,
        text: str,
        action: Callable[[], Any],
        disabled: bool = False,
        image: tkinter.PhotoImage | None = None,
        color: str = consts.COLORS.YELLOW,
        padx: float = 15,
        pady: float = 0,
        fontsize: int = 20,
        **kwargs: Any,
    ) -> None:
        self.color = color
        self.action = action
        self.padx = padx
        self.pady = pady
        self.disabled = disabled
        if disabled:
            color = consts.COLORS.GREY
        super().__init__(parent, color=color, **kwargs)
        self.label = TextLabel(
            self.content,
            text,
            font=consts.FONTS.BUTTON,
            image=image,
            fontsize=fontsize,
            color=color,
        )
        self.label.pack()
        if not disabled:
            self.bind("<Button-1>", lambda _: action())
            self.label.bind("<Button-1>", lambda _: action())

    @property
    def enabled(self) -> bool:
        return not self.disabled

    def enable(self) -> None:
        self.disabled = False
        self.configure(bg=self.color)
        self.label.configure(fg=self.color)
        self.bind("<Button-1>", lambda _: self.action())
        self.label.bind("<Button-1>", lambda _: self.action())

    def disable(self) -> None:
        self.disabled = True
        self.configure(bg=consts.COLORS.GREY)
        self.label.configure(fg=consts.COLORS.GREY)
        self.unbind("<Button-1>")
        self.label.unbind("<Button-1>")


class LoadingFrame(BorderedFrame):
    _c = ["|", "/", "-", "\\"]

    def __init__(
        self,
        parent: tkinter.Misc,
        minsize: float = 0,
        loadersize: int = 40,
        **kwargs: Any,
    ) -> None:
        self.errimg = tkinter.PhotoImage(
            data=resources.RESOURCES.NETERROR_PNG, format="png"
        )
        self.minsize = minsize
        self.loading = False
        self.loading_char = None
        self.loading_label = None
        self.loading_size = loadersize
        super().__init__(parent, **kwargs)
        self.bind_all("<<Reload>>", lambda _: self.start_loading(), "+")
        self.start_loading()

    def stop_loading(self) -> None:
        if not self.loading:
            return
        self.loading_char = None
        self.loading_label.destroy()
        self.loading = False
        self.pack_propagate(False)

    def start_loading(self) -> None:
        if self.loading:
            return
        self.clear()
        self.loading_char = itertools.cycle(LoadingFrame._c)
        self.loading_label = TextLabel(
            self.content,
            "",
            font=consts.FONTS.LOADER,
            fontsize=self.loading_size,
        )
        self.loading = True
        self.update()

    def fail_loading(self) -> None:
        self.clear()
        TextLabel(self, "", image=self.errimg)

    def update(self) -> None:
        if self.loading:
            self.loading_label.configure(text=next(self.loading_char))
            self.loading_label.pack(fill=tkinter.BOTH, expand=True)
            self.after(100, self.update)


# TODO: Last bloc has the wrong color if blocsnumber is not even
class LoadingBarFrame(tkinter.Frame):
    def __init__(
        self,
        parent: tkinter.Misc,
        strartcolor: str = consts.COLORS.RED,
        middlecolor: str = consts.COLORS.YELLOW,
        endcolor: str = consts.COLORS.GREEN,
        blocsnumber: int = 16,
        **kwargs: Any,
    ) -> None:
        super().__init__(parent, padx=5, pady=5, **kwargs)
        self.rawstartcolor = [x // 256 for x in self.winfo_rgb(strartcolor)]
        self.rawmiddlecolor = [x // 256 for x in self.winfo_rgb(middlecolor)]
        self.rawendcolor = [x // 256 for x in self.winfo_rgb(endcolor)]
        self.half = blocsnumber // 2
        self.blocs = [
            tkinter.Frame(
                self,
                bg=self.get_bloccolor(i),
                padx=2,
            )
            for i in range(blocsnumber)
        ]

    def get_bloccolor(self, i: int) -> str:
        if i < self.half:
            startcolor = self.rawstartcolor
            endcolor = self.rawmiddlecolor
        else:
            startcolor = self.rawmiddlecolor
            endcolor = self.rawendcolor
            i %= self.half
        return "#" + str(
            (
                f"{(c[0] * (self.half - i) + (c[1] * i)) // self.half:02x}"
                for c in zip(startcolor, endcolor)
            )
        )
