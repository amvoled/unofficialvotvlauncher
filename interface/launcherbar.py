import tkinter
from typing import Any

import consts
from customtypes import DlInfo
from interface.customwidgets import (
    BorderedButton,
    BorderedFrame,
    LoadingBarFrame,
    LoadingFrame,
    TextLabel,
)


class UpdateStatus(LoadingFrame):
    def __init__(self, parent: tkinter.Misc, **kwargs: Any) -> None:
        super().__init__(parent, **kwargs)
        self.percent = None

    def update_status(self, info: DlInfo) -> None:
        self.stop_loading()
        frame = tkinter.Frame(self)
        TextLabel(
            frame,
            info.name,
            fontsize=15,
            color=consts.COLORS.BLUE,
        )
        TextLabel(
            frame, "released on", fontsize=15
        )
        TextLabel(
            frame,
            info.date,
            fontsize=15,
            color=consts.COLORS.BLUE,
        )

    def start_update(self) -> None:
        self.clear()
        LoadingBarFrame(self)
        TextLabel(
            self,
            "Downloading...",
            color=consts.COLORS.YELLOW,
        )
        TextLabel(self, "100%", color=consts.COLORS.BLUE)
        self.stop_loading()


class LauncherBar(tkinter.Frame):
    def __init__(self, parent: tkinter.Misc) -> None:
        super().__init__(parent, bg=consts.COLORS.BACKGROUND)
        self.columnconfigure(0, uniform="x", weight=1)
        self.columnconfigure(1, uniform="x", weight=1)
        self.rowconfigure(0)
        left = tkinter.Frame(self, bg=consts.COLORS.BACKGROUND)
        left.grid(column=0, row=0, sticky=tkinter.NSEW)
        self.remotever = UpdateStatus(left, loadersize=20)
        self.remotever.pack(expand=True, fill=tkinter.BOTH)
        right = tkinter.Frame(self, bg=consts.COLORS.BACKGROUND)
        right.grid(column=1, row=0, sticky=tkinter.NSEW)
        self.updatebutton = BorderedButton(
            right,
            "Download",
            lambda: self.event_generate("<<Download>>"),
            disabled=True,
            color=consts.COLORS.BLUE,
        )
        self.updatebutton.pack(side=tkinter.LEFT)
        self.localver = BorderedFrame(right)
        TextLabel(
            self.localver,
            "No local version",
            color=consts.COLORS.RED,
            fontsize=15,
        )
        self.localver.pack(side=tkinter.LEFT, expand=True, fill=tkinter.BOTH)
        self.launchbutton = BorderedButton(
            right,
            "Start",
            lambda: self.event_generate("<<Start>>"),
            disabled=True,
        )
        self.launchbutton.pack(side=tkinter.LEFT)
