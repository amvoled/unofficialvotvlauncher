import asyncio
import tkinter.scrolledtext
from typing import Any

import consts
from interface.blog import BlogBrowser, BlogContent, BlogList
from interface.customwidgets import BorderedButton, BorderedFrame, TextLabel
from interface.images import Image
from interface.launcherbar import LauncherBar
from interface.topbar import TopBar


class BottomText(tkinter.Frame):
    def __init__(self, parent: tkinter.Misc, **kwargs: Any) -> None:
        super().__init__(parent, bg=consts.COLORS.BACKGROUND, **kwargs)
        self.label = TextLabel(
            self,
            consts.ABOUT,
            fontsize=10,
            color=consts.COLORS.GREY,
        )
        self.updstatus = TextLabel(
            self, f"Ver [{consts.VERSION}]", fontsize=10
        )
        self.label.pack(side=tkinter.LEFT)
        self.updstatus.pack(side=tkinter.RIGHT)


class MainScreen(BorderedFrame):
    def __init__(self, parent: tkinter.Misc) -> None:
        super().__init__(parent)
        self.top = TopBar(self.content)
        self.top.pack(fill=tkinter.X)
        self.blog = BlogBrowser(self.content)
        self.blog.pack(fill=tkinter.BOTH, expand=True)
        self.launcher = LauncherBar(self.content)
        self.launcher.pack(fill=tkinter.X)


class MainWindow(tkinter.Tk):
    def __init__(self) -> None:
        super().__init__()
        self._config_main_window()
        self.main = MainScreen(self)
        self.credits = BottomText(self)
        self.main.pack(expand=True, fill=tkinter.BOTH)
        self.credits.pack(fill=tkinter.X)
        self.running = True
        self._config_keyboard_shortcuts()

    def _config_main_window(self) -> None:
        self.configure(background=consts.COLORS.BACKGROUND)
        self.iconphoto(True, Image.ICON)
        self.geometry(consts.RESOLUTION)
        self.resizable(False, False)
        self.title(consts.TITLE)
        self.protocol("WM_DELETE_WINDOW", self.stop)
        self.bind("<<Exit>>", lambda _: self.stop())
        self.focus_force()

    def _config_keyboard_shortcuts(self) -> None:
        bloglist = self.main.blog.bloglist
        self.bind("<Return>", lambda _: self.event_generate("<<Start>>"))
        self.bind("<Escape>", lambda _: self.event_generate("<<Exit>>"))
        self.bind("<Down>", lambda _: bloglist.down())
        self.bind("<Up>", lambda _: bloglist.up())
        self.bind("r", lambda _: self.event_generate("<<Reload>>"))

    def stop(self) -> None:
        self.destroy()
        self.running = False

    async def loop(self) -> None:
        while self.running:
            self.update()
            await asyncio.sleep(0.1)
        self.destroy()
        self.quit()
