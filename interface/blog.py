import tkinter
import tkinter.scrolledtext

import consts
from customtypes import BlogPost
from interface.customwidgets import BorderedFrame, LoadingFrame, TextLabel


class BlogContent(LoadingFrame):
    def display_content(self, content: str) -> None:
        self.clear()
        st = tkinter.scrolledtext.ScrolledText(
            self,
            bg=consts.COLORS.BACKGROUND,
            fg=consts.COLORS.WHITE,
            font=consts.FONTS.TEXT,
            wrap=tkinter.WORD,
        )
        st.insert(tkinter.INSERT, content)
        st.configure(state=tkinter.DISABLED, cursor="arrow")
        st.bind("<Button-1>", lambda _: "break")
        st.pack(fill=tkinter.BOTH, expand=True)
        self.stop_loading()


class BlogList(LoadingFrame):
    def __init__(self, parent: tkinter.Misc) -> None:
        super().__init__(parent)
        self.postlist: list[tkinter.Widget] = []
        self.selected = 0

    def select(self, index: int) -> None:
        if (
            self.loading
            or index == self.selected
            or not (0 <= index < len(self.postlist))
        ):
            return
        self.selected = index
        for i, elem in enumerate(self.postlist):
            if i == index:
                elem.configure(bg=consts.COLORS.YELLOW)
            else:
                elem.configure(bg=consts.COLORS.BACKGROUND)
        self.master.event_generate("<<Blog>>")

    def down(self) -> None:
        self.select(self.selected + 1)

    def up(self) -> None:
        self.select(self.selected - 1)

    def update_list(self, posts: list[BlogPost]) -> None:
        self.clear()
        self.postlist = []
        TextLabel(
            self.content,
            text="Latest blog posts:",
            color=consts.COLORS.YELLOW,
        ).pack(anchor=tkinter.NW)
        for i, post in enumerate(posts):
            elem = BorderedFrame(
                self.content,
                size=1,
                pady=0,
                padx=0,
                color=consts.COLORS.BACKGROUND
                if i > 0
                else consts.COLORS.YELLOW,
            )
            title = (
                post.title if len(post.title) < 60 else post.title[:60] + "..."
            )
            titlelb = TextLabel(
                elem.content,
                title,
                fontsize=10,
                color=consts.COLORS.GREEN,
            )
            titlelb.pack()
            datelb = TextLabel(
                elem.content,
                post.date,
                fontsize=10,
                color=consts.COLORS.GREY,
            )
            titlelb.pack()
            elem.pack(fill=tkinter.X)
            for e in [elem, titlelb, datelb]:
                e.bind("<Button-1>", lambda _, target=i: self.select(target))
            self.postlist.append(elem)
        self.stop_loading()
        self.select(0)


class BlogBrowser(tkinter.Frame):
    def __init__(self, parent: tkinter.Misc) -> None:
        super().__init__(parent, bg=consts.COLORS.BACKGROUND)
        self.blog = BlogContent(self)
        self.blog.pack(side=tkinter.LEFT, expand=True, fill=tkinter.BOTH)
        self.bloglist = BlogList(self)
        self.bloglist.pack(side=tkinter.RIGHT, expand=True, fill=tkinter.BOTH)
