import os
import tkinter
import webbrowser

import consts
from interface import BorderedButton, Image


class LinkButtons(tkinter.Frame):
    def __init__(self, parent: tkinter.Misc) -> None:
        super().__init__(parent, bg=consts.COLORS.BACKGROUND)
        self.linkbuttons = []
        for text, url in consts.TOP_BUTTONS:
            button = BorderedButton(
                self,
                text,
                lambda target=url: webbrowser.open(target),
            )
            button.pack(side=tkinter.LEFT)
            self.linkbuttons.append(button)


class TopBar(tkinter.Frame):
    def __init__(self, parent: tkinter.Misc) -> None:
        super().__init__(parent, bg=consts.COLORS.BACKGROUND)
        self.links = LinkButtons(self)
        self.links.pack(side=tkinter.LEFT)
        self.refreshbutton = BorderedButton(
            self,
            "",
            lambda: self.master.event_generate("<<Reload>>"),
            image=Image.REFRESH,
            color=consts.COLORS.BLUE,
        )
        self.refreshbutton.pack(side=tkinter.RIGHT)
        self.folderbutton = BorderedButton(
            self,
            "",
            lambda: os.startfile(consts.PATHS.GAME_PATH),
            image=Image.FOLDER,
            color=consts.COLORS.WHITE,
        )
        self.folderbutton.pack(side=tkinter.RIGHT)
