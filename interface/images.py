import tkinter
from enum import Enum
from typing import Any

import resources


class _LazyImageLoader:
    def __init__(self, resname: str) -> None:
        super().__init__()
        self._data = getattr(resources.RESOURCES, resname)
        self._img: tkinter.PhotoImage | None = None

    def __get__(self, instance: Any, owner: Any) -> tkinter.PhotoImage:
        if self._img is None:
            self._img = tkinter.PhotoImage(data=self._data, format="png")
        return self._img


class Image(tkinter.PhotoImage, Enum):
    ICON = _LazyImageLoader("ICON_PNG")
    FOLDER = _LazyImageLoader("FOLDER_PNG")
    REFRESH = _LazyImageLoader("REFRESH_PNG")
