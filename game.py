import asyncio
import os
import pathlib
import tempfile

import httpx

import consts
import interface
import itchconnector
import resources
import sounds


class GameManager:
    def __init__(self, launcherwindow: interface.LauncherBar) -> None:
        self.infos = None
        self.gamebinary = None
        self.launcherwindow = launcherwindow
        self.search_binary()
        if self.gamebinary is not None:
            launcherwindow.launchbutton.enable()

    def search_binary(self) -> None:
        for root, _, files in os.walk(consts.PATHS.LAUNCHER_GAME_INSTALL):
            for file in files:
                if file == consts.GAME_BIN_NAME:
                    self.gamebinary = os.path.join(root, file)
                    return

    async def download_game(self) -> None:
        with (
            tempfile.NamedTemporaryFile(delete=False) as extractorfile,
            tempfile.NamedTemporaryFile(delete=False) as downloadfile,
        ):
            size = 0
            async for totalsize, chunk in itchconnector.itch.download_latest(
                self.infos.id
            ):
                downloadfile.write(chunk)
                size += len(chunk)
                print(round((size / totalsize) * 100))
            downloadfile.close()
            extractorfile.write(resources.RESOURCES.SZR_EXE)
            extractorfile.close()
            try:
                os.makedirs(consts.PATHS.LAUNCHER_GAME_INSTALL, exist_ok=True)
                extractor = await asyncio.create_subprocess_exec(
                    extractorfile.name,
                    "x",
                    downloadfile.name,
                    "-y",
                    "-bsp1",
                    f"-o{consts.PATHS.LAUNCHER_GAME_INSTALL}",
                    stdout=asyncio.subprocess.PIPE,
                )
                while extractor.returncode is None:
                    try:
                        line = await extractor.stdout.readuntil(b"\r")
                    except asyncio.IncompleteReadError:
                        await extractor.wait()
                    if b"%" in line:
                        print(int(line.split(b"%")[0].strip()))
            finally:
                os.unlink(extractorfile.name)
                os.unlink(downloadfile.name)
        self.search_binary()
        self.launcherwindow.launchbutton.enable()

    def update_game(self) -> None:
        sounds.play_sound(sounds.Sound.BUTTON_OK)
        asyncio.create_task(self.download_game())

    def start_game(self) -> None:
        sounds.play_sound(sounds.Sound.BUTTON_OK)
        os.spawnl(os.P_OVERLAY, pathlib.Path(self.gamebinary), self.gamebinary)

    async def update_latest_version(self) -> None:
        try:
            self.infos = await itchconnector.itch.get_dl_info()
        except httpx.HTTPError:
            # self.launcherwindow.fail_loading()
            raise
        # self.versioninterface.update_status(self.infos)
        self.launcherwindow.updatebutton.enable()
