import asyncio

import devblog
import game
import interface


class Launcher:
    def __init__(self) -> None:
        self.window = interface.MainWindow()
        self.bpmanager = devblog.DevBlogManager(self.window.main.blog)
        self.gmanager = game.GameManager(self.window.main.launcher)
        self.reload()
        self.window.bind("<<Reload>>", lambda _: self.reload())
        self.window.bind("<<Blog>>", lambda _: self.select_blogpost())
        self.window.bind("<<Download>>", lambda _: self.donwload())
        self.window.bind("<<Start>>", lambda _: self.start_game())

    def reload(self) -> None:
        asyncio.create_task(self.bpmanager.update_blogpost_list())
        asyncio.create_task(self.gmanager.update_latest_version())

    def select_blogpost(self) -> None:
        print(f"Selected {self.window.main.blog.bloglist.selected}")

    def donwload(self) -> None:
        print("DOWN")

    def start_game(self) -> None:
        print("START")

    async def loop(self) -> None:
        await self.window.loop()


async def amain() -> None:
    launcher = Launcher()
    await launcher.loop()
    for task in asyncio.all_tasks() - {asyncio.current_task()}:
        task.cancel()
        await asyncio.gather(task, return_exceptions=True)


def main() -> None:
    asyncio.run(amain())


if __name__ == "__main__":
    main()
