import asyncio

import httpx

import interface
import itchconnector
from customtypes import BlogPost


class DevBlogManager:
    def __init__(self, blogwindow: interface.BlogBrowser) -> None:
        self.blogposts: list[BlogPost] = []
        self.cache: dict[str, str] = {}
        self.loadtask: asyncio.Task | None = None
        self.blogwindow = blogwindow

    async def update_blogpost_list(self) -> None:
        try:
            self.blogposts = await itchconnector.itch.get_blogposts()
        except httpx.HTTPError:
            self.blogwindow.bloglist.fail_loading()
            if self.blogwindow.blog.loading:
                self.blogwindow.blog.fail_loading()
            raise
        self.blogwindow.bloglist.update_list(self.blogposts)

    async def fetch_blogpost(self, index: int) -> None:
        post = self.blogposts[index]
        if post.url in self.cache:
            content = self.cache[post.url]
        else:
            self.blogwindow.blog.start_loading()
            try:
                content = await itchconnector.itch.get_blogspot_content(post)
            except httpx.HTTPError:
                self.blogwindow.blog.fail_loading()
                raise
            self.cache[post.url] = content
        content = content.replace("\n\n", "\n")
        self.blogwindow.blog.display_content(content)
        self.loadtask = None

    def load_blogpost(self, index: int) -> None:
        if self.loadtask is not None:
            self.loadtask.cancel()
        self.loadtask = asyncio.create_task(self.fetch_blogpost(index))
