import collections
import enum
import json
from typing import Any, Iterator

import consts


class Setting(enum.StrEnum):
    INST_GAME_ID = "instgameid"
    INST_GAME_NAME = "instgamename"


class ConfigFile(collections.MutableMapping):
    def __init__(self) -> None:
        self._data: dict[str, Any] = {}

    def _read(self) -> None:
        with open(consts.PATHS.LAUNCHER_CONFIG, "r") as file:
            self._data = json.load(file)

    def _write(self) -> None:
        with open(consts.PATHS.LAUNCHER_CONFIG, "w") as file:
            json.dump(self._data, file)

    def __setitem__(self, key: str, value: Any) -> None:
        self._data[key] = value
        self._write()

    def __delitem__(self, key: str) -> None:
        if key in self._data:
            del self._data[key]
            self._write()

    def __getitem__(self, key: str) -> Any:
        if key not in self._data:
            self._read()
        return self._data[key]

    def __len__(self) -> int:
        return len(self._data)

    def __iter__(self) -> Iterator:
        return iter(self._data)


config = ConfigFile()
