import os
import sys

VERSION = "0.0.1"


# Paths used
class PATHS:
    BASE_PATH = getattr(sys, "_MEIPASS", os.getcwd())
    ICON = os.path.join(BASE_PATH, "icon.ico")
    LOCALAPPDATA = os.path.expandvars("%localappdata%")
    GAME_PATH = os.path.join(LOCALAPPDATA, "votv")
    LAUNCHER_PATH = os.path.join(GAME_PATH, "UnofficialLauncher")
    LAUNCHER_CONFIG = os.path.join(LAUNCHER_PATH, "config.json")
    LAUNCHER_GAME_INSTALL = os.path.join(LAUNCHER_PATH, "gameinstall")


GAME_BIN_NAME = "VotV.exe"


# Urls for buttons. The ITCH url is also used for api requests
class URL:
    ITCH = "https://mrdrnose.itch.io/votv"
    PATREON = "https://www.patreon.com/eternitydev"
    DISCORD = "https://discord.gg/WKBvqu4tjV"
    DALERT = "https://www.donationalerts.com/r/mrdrnose"
    TWITTER = "https://twitter.com/MrDrNose1"
    YOUTUBE = "https://www.youtube.com/channel/UCthmkgnVs7mohAakF4ucjlQ"


# List of buttons at the top, list of tuples 'text', 'url'
TOP_BUTTONS = [
    ("Itch.io", URL.ITCH),
    ("Patreon", URL.PATREON),
    ("Discord", URL.DISCORD),
    ("Donation Alerts", URL.DALERT),
    ("Twitter", URL.TWITTER),
    ("Youtube", URL.YOUTUBE),
]


# UI Colors, can use tkinter colors or hex in format #ffffff
class COLORS:
    WHITE = "white"
    RED = "red"
    YELLOW = "yellow"
    GREEN = "green2"
    BLUE = "cyan"
    GREY = "grey"
    BACKGROUND = "black"


# UI Fonts
class FONTS:
    LABEL = "System"
    BUTTON = "System"
    LOADER = "Terminal"
    TEXT = "Courier"


# UI resolution
RESOLUTION = "1280x720"

# UI window title
TITLE = "VotV Unofficial Launcher"

# UI "legal" text
ABOUT = (
    "This launcher is not official and not affiliated with the original "
    "creator of Voices of the Void, EternityDev. Made by Amavoleda. "
    "Contact me on the VotV Discord if you have any issues :)"
)
