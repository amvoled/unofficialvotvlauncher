import abc
import os
from enum import Enum

import resources


class Sound(Enum):
    BUTTON_OK = resources.RESOURCES.BUTTONOK_WAV


class _SoundPlayer(abc.ABC):
    @abc.abstractmethod
    def play(self, data: bytes) -> None:
        ...


class _WinSound(_SoundPlayer):
    def __init__(self) -> None:
        import winsound

        self._playsound = winsound.PlaySound
        self._flags = winsound.SND_MEMORY | winsound.SND_NOSTOP

    def play(self, data: bytes) -> None:
        self._playsound(data, self._flags)


_player: _SoundPlayer | None
match os.name:
    case "nt":
        _player = _WinSound()
    case _:
        _player = None


def play_sound(sound: Sound) -> None:
    if _player is not None:
        _player.play(sound.value)
