from typing import Generator

import bs4
import httpx

import consts
from customtypes import BlogPost, DlInfo


class ItchConnector:
    headers = {"User-Agent": f"VotV Unofficial Launcher {consts.VERSION}"}

    def __init__(self) -> None:
        self.client = httpx.AsyncClient(
            base_url=consts.URL.ITCH, headers=self.headers
        )

    async def get_dl_info(self) -> DlInfo:
        dlpagereq = await self.client.post("/download_url")
        dlpage = await self.client.get(dlpagereq.json()["url"])
        dlsoup = bs4.BeautifulSoup(dlpage.text, "html.parser")
        dlid = dlsoup.find("a", class_="button download_btn").attrs[
            "data-upload_id"
        ]
        dlname = dlsoup.find("strong", class_="name").text
        dlsize = dlsoup.find("span", class_="file_size").text
        dldate = (
            dlsoup.find("div", class_="upload_date").find("abbr").get("title")
        )
        return DlInfo(dlid, dlname, dldate, dlsize)

    async def get_blogposts(self) -> list[BlogPost]:
        status = await self.client.get("/devlog")
        statussoup = bs4.BeautifulSoup(status.text, "html.parser")
        blogposts = []
        for tag in (
            statussoup.find("div", class_="devlog").find("ul").find_all("li")
        ):
            atag = tag.find("a", class_="title")
            title = atag.text
            url = atag.get("href")
            date = tag.find("abbr").get("title")
            blogposts.append(BlogPost(title, date, url))
        return blogposts

    async def get_blogspot_content(self, blogpost: BlogPost) -> str:
        content = await self.client.get(blogpost.url)
        contentsoup = bs4.BeautifulSoup(content.text, "html.parser")
        contenttext = contentsoup.find("section", class_="post_body").get_text(
            "\n"
        )
        return contenttext

    async def download_latest(self, dlid: str) -> Generator:
        dlreq = await self.client.post(consts.URL.ITCH + f"/file/{dlid}")
        url = dlreq.json()["url"]
        async with self.client.stream("GET", url) as response:
            total_size = int(response.headers["content-length"])
            async for chunk in response.aiter_bytes():
                yield total_size, chunk


itch = ItchConnector()
